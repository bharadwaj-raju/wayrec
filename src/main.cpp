/*
  SPDX-FileCopyrightText: 2021 Bharadwaj Raju <bharadwaj.raju777@protonmail.com>

  SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QUrl>
#include <QDesktopServices>
#include <QMenu>
#include <QStandardPaths>
#include <QDesktopServices>

#include <KLocalizedContext>
#include <KLocalizedString>
#include <KDeclarative/KDeclarative>
#include <KQuickAddons/QtQuickSettings>

#include "backend.h"

int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    KLocalizedString::setApplicationDomain("wayrec");
    QCoreApplication::setOrganizationName("KDE");
    QCoreApplication::setOrganizationDomain("kde.org");
    QCoreApplication::setApplicationName("WayRec");

    KQuickAddons::QtQuickSettings::init();


    QQmlApplicationEngine engine;

    Backend backend;
    qmlRegisterSingletonInstance<Backend>("org.kde.wayrec", 1, 0, "Backend", &backend);

    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}

