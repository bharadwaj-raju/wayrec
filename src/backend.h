/*
  SPDX-FileCopyrightText: 2021 Bharadwaj Raju <bharadwaj.raju777@protonmail.com>

  SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/


#pragma once

#include <QObject>

#include <QDBusArgument>
#include <QDBusConnection>
#include <QDBusConnectionInterface>
#include <QDBusUnixFileDescriptor>
#include <QFileDialog>
#include <QDBusObjectPath>
#include <QFlags>
#include <QDebug>

#include <gst/gst.h>

typedef struct _CustomData {
    GstElement *pipeline;
    GstElement *identity;
    GstClock   *clock;
} CustomData;

class Backend : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool readyToStream READ readyToStream NOTIFY readyToStreamChanged)
    Q_PROPERTY(QString saveFile READ saveFile NOTIFY saveFileChanged)
    Q_PROPERTY(uint fps READ fps WRITE setFps NOTIFY fpsChanged)
    Q_PROPERTY(QString encodingPreset READ encodingPreset WRITE setEncodingPreset NOTIFY encodingPresetChanged)

public:
    explicit Backend(QObject *parent = nullptr);
    Q_SLOT void requestScreenSharing();
    Q_SLOT void stopRecording();
    Q_SLOT void startRecording();
    Q_SLOT void chooseSaveFile();
    bool readyToStream();
    QString saveFile();
    uint fps();
    Q_SLOT void setFps(uint fps);
    QString encodingPreset();
    Q_SLOT void setEncodingPreset(QString encodingPreset);
    typedef struct {
        uint node_id;
        QVariantMap map;
    } Stream;
    typedef QList<Stream> Streams;
    CustomData m_gstData;

signals:
    void readyToStreamChanged();
    void saveFileChanged();
    void fpsChanged();
    void encodingPresetChanged();
    void errorMsg(QString msg);

private:
    QString m_saveFile;
    bool m_readyToStream;
    Streams m_streams;
    QString m_portalDBusName;
    QString m_portalDBusIFace;
    uint m_sessionToken;
    uint m_requestToken;
    uint m_fps = 25;
    QString m_encodingPreset = "superfast";
    QString m_sessionHdl;
    GstElement *m_element;
    Q_SLOT void sessionCreated(uint response, const QVariantMap &results);
    Q_SLOT void sourcesSelected(uint response, const QVariantMap &results);
    Q_SLOT void screenCastStarted(uint response, const QVariantMap &results);
};
