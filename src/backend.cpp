/*
  SPDX-FileCopyrightText: 2021 Bharadwaj Raju <bharadwaj.raju777@protonmail.com>

  SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include "backend.h"

Q_DECLARE_METATYPE(Backend::Stream);
Q_DECLARE_METATYPE(Backend::Streams);

Backend::Backend(QObject *parent)
: QObject(parent), m_sessionToken(0), m_requestToken(0)
{
    gst_init(nullptr, nullptr);

    m_readyToStream = false;

    m_portalDBusName = "org.freedesktop.portal.Desktop";
    m_portalDBusIFace = "org.freedesktop.portal.ScreenCast";
}

const QDBusArgument &operator >> (const QDBusArgument &arg, Backend::Stream &stream)
{
    arg.beginStructure();
    arg >> stream.node_id;

    arg.beginMap();
    while (!arg.atEnd()) {
        QString key;
        QVariant map;
        arg.beginMapEntry();
        arg >> key >> map;
        arg.endMapEntry();
        stream.map.insert(key, map);
    }
    arg.endMap();
    arg.endStructure();

    return arg;
}

bool Backend::readyToStream() {
    return m_readyToStream;
}

QString Backend::saveFile() {
    return m_saveFile;
}

void Backend::chooseSaveFile()
{
    m_saveFile = QFileDialog::getSaveFileName(nullptr, "", "", tr("Matroska video (*.mkv)"));
    emit saveFileChanged();
}

uint Backend::fps() {
    return m_fps;
}

void Backend::setFps(uint fps) {
    if (m_fps != fps) {
        m_fps = fps;
        emit fpsChanged();
    }
}

QString Backend::encodingPreset() {
    return m_encodingPreset;
}

void Backend::setEncodingPreset(QString encodingPreset) {
    if (m_encodingPreset != encodingPreset) {
        m_encodingPreset = encodingPreset;
        emit encodingPresetChanged();
    }
}

void Backend::requestScreenSharing()
{
    QDBusMessage message = QDBusMessage::createMethodCall(m_portalDBusName,
                                                          QLatin1String("/org/freedesktop/portal/desktop"),
                                                          m_portalDBusIFace,
                                                          QLatin1String("CreateSession"));
    m_sessionToken++;
    m_requestToken++;
    message << QVariantMap { { QLatin1String("session_handle_token"), QString("u%1").arg(m_sessionToken) }, { QLatin1String("handle_token"), QString("u%1").arg(m_requestToken) } };

    QDBusPendingCall pendingCall = QDBusConnection::sessionBus().asyncCall(message);
    QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(pendingCall);
    connect(watcher, &QDBusPendingCallWatcher::finished, [this] (QDBusPendingCallWatcher *watcher) {
        QDBusPendingReply<QDBusObjectPath> reply = *watcher;
        if (reply.isError()) {
            qWarning() << "Couldn't get reply";
            qWarning() << "Error: " << reply.error().message();
            /* Workaround: Neon's (Ubuntu LTS) build of xdg-desktop-portal isn't built with PipeWire support
             * so it doesn't have the ScreenCast interface even though the platform implementation may support it.
             * To make this work on KDE Neon, we'll instead use the platform implementation directly */
            if (m_portalDBusName == "org.freedesktop.portal.Desktop" && reply.error().type() == QDBusError::UnknownMethod) {
                qWarning() << "Trying workaround for KDE Neon...";
                m_portalDBusName = "org.freedesktop.impl.portal.desktop.kde";
                m_portalDBusIFace = "org.freedesktop.impl.portal.ScreenCast";
                requestScreenSharing();
                return;
            }
        }
        else {
            QDBusConnection::sessionBus().connect(QString(), reply.value().path(),
                QLatin1String("org.freedesktop.portal.Request"), QLatin1String("Response"),
                this, SLOT(sessionCreated(uint, QVariantMap))
            );
        }
    });
}

void Backend::sessionCreated(uint response, const QVariantMap &results)
{
    if (response != 0) {
        qWarning() << "Failed to create session: " << response;
        return;
    }

    QDBusMessage message = QDBusMessage::createMethodCall(m_portalDBusName,
                                                          QLatin1String("/org/freedesktop/portal/desktop"),
                                                          m_portalDBusIFace,
                                                          QLatin1String("SelectSources"));

    m_sessionHdl = results.value(QLatin1String("session_handle")).toString();
    qDebug() << m_sessionHdl;
    m_requestToken++;
    message << QVariant::fromValue(QDBusObjectPath(m_sessionHdl))
    << QVariantMap { { QLatin1String("multiple"), false},
    // right now window capture support needs some more work -- how to find width/height?
    { QLatin1String("types"), (uint)(1) /* bitmask, 1 = monitor, 2 = window, 1 | 2 = all types */},
    { QLatin1String("handle_token"), QString("u%1").arg(m_requestToken) } };

    QDBusPendingCall pendingCall = QDBusConnection::sessionBus().asyncCall(message);
    QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(pendingCall);
    connect(watcher, &QDBusPendingCallWatcher::finished, [this] (QDBusPendingCallWatcher *watcher) {
        QDBusPendingReply<QDBusObjectPath> reply = *watcher;
        if (reply.isError()) {
            qWarning() << "SelectSources't get reply";
            qWarning() << "Error: " << reply.error().message();
        }
        else {
            QDBusConnection::sessionBus().connect(QString(), reply.value().path(),
                QLatin1String("org.freedesktop.portal.Request"), QLatin1String("Response"),
                this, SLOT(sourcesSelected(uint, QVariantMap)));
        }
    });
}

void Backend::sourcesSelected(uint response, const QVariantMap &results)
{
    if (response != 0) {
        qWarning() << "Failed to select sources: " << response;
        return;
    }

    QDBusMessage message = QDBusMessage::createMethodCall(m_portalDBusName,
                                                          QLatin1String("/org/freedesktop/portal/desktop"),
                                                          m_portalDBusIFace,
                                                          QLatin1String("Start"));

    m_requestToken++;
    message << QVariant::fromValue(QDBusObjectPath(m_sessionHdl))
    << QString() // parent_window
    << QVariantMap { { QLatin1String("handle_token"), QString("u%1").arg(m_requestToken) } };

    QDBusPendingCall pendingCall = QDBusConnection::sessionBus().asyncCall(message);
    QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(pendingCall);
    connect(watcher, &QDBusPendingCallWatcher::finished, [this] (QDBusPendingCallWatcher *watcher) {
        QDBusPendingReply<QDBusObjectPath> reply = *watcher;
        if (reply.isError()) {
            qWarning() << "Couldn't get reply";
            qWarning() << "Error: " << reply.error().message();
        } else {
            QDBusConnection::sessionBus().connect(QString(), reply.value().path(),
                QLatin1String("org.freedesktop.portal.Request"), QLatin1String("Response"),
                this, SLOT(screenCastStarted(uint, QVariantMap)));
        }
    });
}

void Backend::screenCastStarted(uint response, const QVariantMap &results)
{
    if (response != 0) {
        qWarning() << "Failed to start: " << response;
    }

    m_streams = qdbus_cast<Streams>(results.value(QLatin1String("streams")));
    m_readyToStream = true;
    emit readyToStreamChanged();
}

void Backend::startRecording() {
    Q_FOREACH (Stream stream, m_streams) {
        QDBusMessage message = QDBusMessage::createMethodCall(m_portalDBusName,
                                                              QLatin1String("/org/freedesktop/portal/desktop"),
                                                              m_portalDBusIFace,
                                                              QLatin1String("OpenPipeWireRemote"));

        message << QVariant::fromValue(QDBusObjectPath(m_sessionHdl)) << QVariantMap();

        QDBusPendingCall pendingCall = QDBusConnection::sessionBus().asyncCall(message);
        pendingCall.waitForFinished();
        QDBusPendingReply<QDBusUnixFileDescriptor> reply = pendingCall.reply();
        if (reply.isError()) {
            qWarning() << "Failed to get fd for node_id " << stream.node_id;
        }
        qDebug() << stream.node_id;
        int w, h;
        QDBusArgument sizeArg = stream.map.value("size").value<QDBusArgument>();
        // this isn't set for windows for some reason, only for monitors
        sizeArg.beginArray();
        sizeArg >> w >> h;
        sizeArg.endArray();
        qDebug() << w << h;

        QString gstLaunch = QString("pipewiresrc fd=%1 path=%2 do-timestamp=TRUE ! videoparse width=%3 height=%4 format=bgra framerate=%5/1 ! videoconvert ! x264enc speed-preset=%6 ! matroskamux ! filesink location=%7")
            .arg(reply.value().fileDescriptor()).arg(stream.node_id).arg(w).arg(h).arg(m_fps).arg(m_encodingPreset).arg(m_saveFile);
        m_gstData.pipeline = gst_parse_launch(gstLaunch.toUtf8(), nullptr);
        gst_element_set_state(m_gstData.pipeline, GST_STATE_PLAYING);
    }
}

void Backend::stopRecording() {
    QDBusMessage message = QDBusMessage::createMethodCall(m_portalDBusName,
                                                          m_sessionHdl,
                                                          QLatin1String("org.freedesktop.portal.Session"),
                                                          QLatin1String("Close"));
    QDBusPendingCall pendingCall = QDBusConnection::sessionBus().asyncCall(message);
    pendingCall.waitForFinished();
}
