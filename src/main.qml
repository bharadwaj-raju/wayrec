/*
  SPDX-FileCopyrightText: 2021 Bharadwaj Raju <bharadwaj.raju777@protonmail.com>

  SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

import QtQuick 2.6
import QtQuick.Controls 2.0 as QQC2
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.13 as Kirigami

import org.kde.wayrec 1.0

Kirigami.ApplicationWindow {
    id: root
    title: i18nc("@title:window", "WayRec")

    width: 500
    height: colView.implicitHeight

    property int secondsSinceRecord: 0
    property int delaySeconds: 0
    property string state: "init"

    ColumnLayout {
        Layout.fillWidth: true
        id: colView
        anchors {
            left: parent.left
            right: parent.right
            leftMargin: Kirigami.Units.gridUnit
            rightMargin: Kirigami.Units.gridUnit
            bottom: Kirigami.Settings.isMobile ? parent.bottom : undefined
        }

        ColumnLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter
            Layout.topMargin: Kirigami.Units.gridUnit
            RowLayout {
                Layout.fillWidth: true
                QQC2.Label {
                    text: Backend.saveFile ? Backend.saveFile : i18n("Choose a file to save to.")
                    elide: Qt.ElideMiddle
                    Layout.fillWidth: true
                }
                QQC2.Button {
                    text: i18n("Save as")
                    onClicked: Backend.chooseSaveFile()
                    Layout.fillWidth: false
                    Layout.alignment: Qt.AlignRight
                }
            }
            RowLayout {
                Layout.fillWidth: true
                QQC2.Label {
                    text: Backend.readyToStream ? i18n("Screen or window selected!") : i18n("Choose a screen or window to record.")
                    Layout.fillWidth: true
                }
                QQC2.Button {
                    text: i18n("Select")
                    onClicked: Backend.requestScreenSharing()
                    Layout.fillWidth: false
                    Layout.alignment: Qt.AlignRight
                }
            }
            RowLayout {
                Layout.fillWidth: true
                QQC2.Label {
                    text: i18n("Delay")
                    Layout.fillWidth: true
                }
                QQC2.SpinBox {
                    id: delayField
                    onValueChanged: {
                        delaySeconds = value
                    }
                }
            }
            RowLayout {
                Layout.fillWidth: true
                QQC2.Label {
                    text: i18n("FPS")
                    Layout.fillWidth: true
                }
                QQC2.SpinBox {
                    value: Backend.fps
                    onValueChanged: {
                        Backend.setFps(value);
                    }
                }
            }
            RowLayout {
                Layout.fillWidth: true
                Layout.bottomMargin: Kirigami.Units.gridUnit * 2
                QQC2.Label {
                    text: i18n("Encoding speed")
                    Layout.fillWidth: true
                }
                QQC2.ComboBox {
                    model: ["superfast", "veryfast", "fast", "medium"]
                    onActivated: {
                        Backend.setEncodingPreset(currentValue);
                    }
                }
            }
            QQC2.ToolButton {
                text: root.state == "init" ? i18n("Record") : (root.state == "delay-waiting" ? (delayTimer.running ? i18n("Cancel") : i18n("Record")) : i18n("Stop"))
                icon.name: "media-record"
                flat: false
                enabled: Backend.readyToStream && Backend.saveFile
                onClicked: {
                    if (root.state == "init") {
                        if (delayField.value == 0) {
                            root.state = "recording"
                            Backend.startRecording()
                            secondsTimer.start()
                        } else {
                            root.state = "delay-waiting"
                            delaySeconds = delayField.value
                            delayTimer.start()
                        }
                    } else if (root.state == "delay-waiting") {
                        if (delayTimer.running) {
                            delayTimer.stop()
                        } else {
                            delayTimer.start()
                        }
                    } else if (root.state == "recording") {
                        Backend.stopRecording()
                        root.state = "recording-done"
                        secondsTimer.stop()
                    }
                }
                Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
                Layout.fillWidth: false
                Layout.bottomMargin: Kirigami.Units.gridUnit*2
            }
            Kirigami.Heading {
                text: (root.state == "recording" || root.state == "recording-done") ? secondsSinceRecord : delaySeconds
                color: (root.state == "recording" || root.state == "recording-done") ? Kirigami.Theme.negativeTextColor : Kirigami.Theme.positiveTextColor
                font.bold: true
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.Wrap
                Layout.fillWidth: true
                Layout.bottomMargin: Kirigami.Units.gridUnit
            }

            Timer {
                id: secondsTimer
                interval: 1000
                repeat: true
                onTriggered: {
                    secondsSinceRecord++;
                }
            }

            Timer {
                id: delayTimer
                interval: 1000
                repeat: true
                onTriggered: {
                    delaySeconds--;
                    if (delaySeconds == 0) {
                        root.state = "recording"
                        Backend.startRecording()
                        secondsTimer.start()
                        delayTimer.stop()
                    }
                }
            }
        }
    }
}
