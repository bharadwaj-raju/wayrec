# WayRec

Simple Wayland screen recorder. Works on ~all desktop environments.

## Building

Build requirements:

  - CMake
  - KDE Frameworks 5.82
  - Qt 5.15
  - GStreamer 1.0

Runtime requirements:

  - Pipewire
  - An xdg-desktop-portal implementation (like -kde, -wlr, -gtk etc)
  - GStreamer Pipewire plugin

```bash
$ cmake -B build .
$ cmake --build build
$ ./build/bin/wayrec
```
